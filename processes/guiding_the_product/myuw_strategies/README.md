One key perspective that guides MyUW product management is the strategies identified in the [MyUW Vision, Mission, and Strategies document][].

> The following strategies advance the MyUW mission: 
 * Focus on useful, relevant, personalized content
 * Help users complete tasks simply and efficiently
 * Respond promptly to feedback and provide great customer service
 * Be agile, innovative, and provide continual improvement
 * Ensure ongoing maintenance and support
 * Foster ongoing relationships and collaboration with campus partners
 * Provide standards and guidelines for consistent design and functionality
 * Provide a flexible and easy-to-use development framework
 * Encourage reusable services and improvements to backend systems

# Expounding upon the strategies

## Focus on useful, relevant, personalized content

### Instrument for analysis

One important measure of usefulness is use. **MyUW instruments content so that its use is measurable.**

### Retire un-useful content

**MyUW retires content that is not useful.**

Largely, that's the same thing as retiring un-used content. Sometimes the potential usefulness in content is not being realized because of a broken user experience or poor communication. Sometimes un-used content can become used through improvement. 

### Show relevant content

MyUW knows something about the user, and uses this to inform what content to suggest.

MyUW optimizes searches so that **the most common searches match the most relevant content for those queries**.

### Hide irrelevant content

MyUW filters so that **content not likely to be useful for the viewing user is de-emphasized.**

### Prefer personalized content

MyUW is a task-oriented personalized gateway to relevant opportunities.

**MyUW prefers and prioritizes content that is itself personalized to the viewing user.**

Examples of personalized content:

 * Employee W-2 delivery
 * Student course dashboard
 * Scholarships dashboard
 * Advising content specifically about an advisor's advisees or about being a particular advisee

### De-emphasize generic static content

MyUW is not a website and is not a web content management system. MyUW de-emphasizes content that's the same for everyone.

This means that MyUW isn't a go-to place to put a departmental web page, a statement of policy, or a how-to article.





[MyUW Vision, Mission, and Strategies document]: https://docs.google.com/document/d/1ZbOwUowOKr9jOlbEY7pLkqboh9UzazjGcIkoFQbNTa0/edit
