# What is this

This repository increases transparency and collaboration potential in MyUW product backlog management by documenting process associated with product backlog management in MyUW.

# What is here?
 
 Processes. Processes are patterns, standard operating procedures, ongoing aspects of MyUW. You can stop doing them, but you cannot complete them.
 
 * [`processes`](./processes/)
 
# What is not here?
 
 Projects. Projects have a beginning, a middle, and an end. You can be "done" with then. The specific open loops that the product backlog management processes described here are applied to should probably be written down, but they're not in this repo.
 
 TODO: Parallel repo with differential access control?
 
 
# Cautions

The [Agile Manifesto][] would have us value

> * **Individuals and interactions** over processes and tools

> * **Working software** over comprehensive documentation

> * **Customer collaboration** over contract negotiation

> * **Responding to change** over following a plan

This repo tends towards processes and tools.

Use it in good health. Frequent fresh air and interactions with motivated individuals and stakeholders are highly recommended.

(See also [philosophy](./philosophy/)).

[Agile Manifesto]: http://agilemanifesto.org/
[Agile Manifesto Principles]: http://agilemanifesto.org/principles.html
